# Docker

The docker-compose provides an app container for Mobilizon itself and a database container for PostgreSQL (Postgis). You'll need your own reverse-proxy to handle TLS termination (on port 4000).

See [documentation](https://docs.joinmobilizon.org/administration/install/docker/) for usage.
