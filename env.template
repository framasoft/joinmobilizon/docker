# Copy this file to .env, then update it with your own settings


######################################################
# Instance configuration                             #
######################################################

# The name for your instance
MOBILIZON_INSTANCE_NAME=My Mobilizon Instance

# Your domain
MOBILIZON_INSTANCE_HOST=mobilizon.lan

# The IP to listen on (defaults to 0.0.0.0)
# MOBILIZON_INSTANCE_LISTEN_IP

# The port to listen on (defaults to 4000). Point your reverse proxy on this port.
MOBILIZON_INSTANCE_PORT=4000

# Whether registrations are opened or closed. Can be changed in the admin settings UI as well.
# Make sure to moderate actively your instance if registrations are opened.
MOBILIZON_INSTANCE_REGISTRATIONS_OPEN=false

# From which email will the emails be sent
MOBILIZON_INSTANCE_EMAIL=noreply@mobilizon.lan

# To which email with the replies be sent
MOBILIZON_REPLY_EMAIL=contact@mobilizon.lan

# The loglevel setting.
# You can find accepted values here: https://hexdocs.pm/logger/Logger.html#module-levels
# Defaults to error
MOBILIZON_LOGLEVEL=error

######################################################
# Database settings                                  #
######################################################

# The values below will be given to both the PostGIS (PostgreSQL) and Mobilizon containers
# Use the next settings if you plan to use an existing external database

# The PostgreSQL username
POSTGRES_USER=mobilizon

# The PostgreSQL password
POSTGRES_PASSWORD=changethis

# The PostgreSQL database name
POSTGRES_DB=mobilizon

# The Mobilizon Database username. Defaults to $POSTGRES_USER.
# Change if using an external database.
# MOBILIZON_DATABASE_USERNAME=

# The Mobilizon Database password. Defaults to $POSTGRES_PASSWORD.
# Change if using an external database.
# MOBILIZON_DATABASE_PASSWORD=

# The Mobilizon Database name. Defaults to $POSTGRES_DB.
# Change if using an external database.
# MOBILIZON_DATABASE_DBNAME=

# The Mobilizon database host. Useful if using an external database.
# MOBILIZON_DATABASE_HOST=

# The Mobilizon database port. Useful if using an external database.
# MOBILIZON_DATABASE_PORT=

# Whether to use SSL to connect to the Mobilizon database. Useful if using an external database.
# MOBILIZON_DATABASE_SSL=

######################################################
# Secrets                                            #
######################################################

# A secret key used as a base to generate secrets for encrypting and signing data.
# Make sure it's long enough (~64 characters should be fine)
# You can run `openssl rand -base64 48` to generate such a secret
MOBILIZON_INSTANCE_SECRET_KEY_BASE=changethis

# A secret key used as a base to generate JWT tokens
# Make sure it's long enough (~64 characters should be fine)
# You can run `openssl rand -base64 48` to generate such a secret
MOBILIZON_INSTANCE_SECRET_KEY=changethis


######################################################
# Email settings                                     #
######################################################

# The SMTP server
# Defaults to localhost
MOBILIZON_SMTP_SERVER=localhost

# The SMTP port
# Usual values: 25, 465, 587
# If using a local mail server, make sure the appropriate port is exposed in the docker-compose configuration as well
# Defaults to 25
MOBILIZON_SMTP_PORT=25

# The SMTP username
# Defaults to nil
MOBILIZON_SMTP_USERNAME=noreply@mobilizon.lan

# The SMTP password
# Defaults to nil
MOBILIZON_SMTP_PASSWORD=password

# Whether to use SSL for SMTP.
# Boolean
# Defaults to false
MOBILIZON_SMTP_SSL=false

# Whether to use TLS for SMTP.
# Allowed values: always (TLS), never (Clear) and if_available (STARTTLS)
# Make sure to match the port value as well
# Defaults to "if_available"
MOBILIZON_SMTP_TLS=if_available
